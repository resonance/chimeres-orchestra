# Setup Chimères Orchestra


## TODO
- Udoo clock
- sd card img

## IP

**MUM**
- IP_MUM 192.168.0.101 9001 (install.pd)

**CHILDREN**
- IP_CHILD 0 192.168.0.101 9101 (child-serial.ino + install.pd ou children.pd)
- IP_CHILD 1 192.168.0.102 9102 (child-osc.ino)
- IP_CHILD 2 192.168.0.103 9103 (child-osc.ino)
- IP_CHILD 3 192.168.0.104 9104 (child-osc.ino)
- IP_CHILD 4 192.168.0.105 9105 (child-osc.ino)
- IP_CHILD 5 192.168.0.106 9106 (child-osc.ino)

## Paramétrer le routeur
- Appuyer sur reset pour remettre la config par défaut
- IP du routeur : 192.168.0.1
- Plage d'adresse : limiter à 10 adresses de 192.168.0.100 à 192.168.0.110 par exemple
- Paramétrer réseau wifi : xxxx, mdp: xxxx
- Paramétrer accès admin filaire : xxxx, xxxx

**Routeur Teltonika RUT950**
- Désactiver le port WAN filaire (wired) : Menu Réseau > WAN. Sélectionner le WAN/Wi-Fi. Sauvegarder.
- Activer le port WAN comme un LAN : Menu Réseau > LAN > Configuration > Paramètres avancées. Cocher "Use WAN port as LAN". Sauvegarder.

## Installation de UDOObuntu (UDOO Quad)
- Documentation UDOO Quad : https://www.udoo.org/docs/Introduction/Introduction.html
- Power 12V/2A
- Pins 3.3V
- Télécharger UDOObuntu 2.2.0 : [udoobuntu-udoo-qdl-desktop_2.2.0.zip](https://sourceforge.net/projects/udooboard/files/UDOO_QUAD-DUAL/UDOObuntu/udoobuntu-udoo-qdl-desktop_2.2.0.zip/download)
- Flasher SD card classe 10 (ou avec Etcher): 
	- df -h
	- sudo umount /dev/mmcblk0p1
	- sudo dd bs=1M if=udoobuntu-udoo-qdl-desktop_2.2.0.img of=/dev/mmcblk0
	- sudo sync

## Configuration UDOObuntu

### Mise à jour
- sudo apt-get update
- sudo apt-get upgrade
- sudo apt-get dist-upgrade
- sudo reboot

### Web Control Panel
Possibilité de configurer la Udoo avec le "Web Control Panel", à distance. Ouvrir le navigateur, mettre l'adresse IP de la UDOO

- Preferences > Udoo Web Panel
	- Région & Language
	- Utilisateur : udooer/xxxx

### Réseau
- sudo nmcli con list
- nmcli dev status
- marche pas : sudo nmcli con up <WIFI_AP>

### Clavier
- Icone US sur le tableau de bord > Keyboard layout Handler settings

### Adresse IP statique
(Preferences > Network Connections si besoin)

- modifier ce fichier : leafpad /etc/network/interfaces

```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
address 192.168.0.101
netmask 255.255.255.0
network 192.168.0.1
broadcast 192.168.0.255
gateway 192.168.0.1

allow-hotplug usb0
iface usb0 inet static
address 192.168.7.2
netmask 255.255.255.252
```

Tests réseau : 
- ping 192.168.0.101
- ssh -X udooer@192.168.0.101

### Temps (RTC)
Il y a une horloge intégrée pour garder l'heure. Il faut connecter une pile 3V entre le bouton "reset (RST)" et l'emplacement de la SD card. En se connectant à Internet, l'horloge se règle.

La façon manuelle :
- vérifier la date sur un ordinateur : "date" ou "watch -n 1 date"
- date -s "2 DEC 2021 13:14:30" (définir la date de façon manuelle)
- sudo hwclock -w (écrire la date dans l'horloge)
- sudo hwclock -r (lire le résultat)
- redémarrer pour tester

### Démarrage
- leafpad ~/.config/lxsession/Lubuntu/autostart
- Ajouter le script de démarrage dans ce fichier : "bash /home/udooer/chimeres-orchestra/software/scripts/start_udoo.sh &"

### VNC
- Sur la UDOO, rien à faire, le serveur VNC est déjà installé
- Sur l'ordinateur distant (sinon avec Remmina installé par défaut) :
	- sudo apt-get install xvnc4viewer
	- sudo xvncviewer
	- Server: 192.168.0.101:5900
	
### Aliases
Quelques alias pour aller plus vite en ligne de commande :
- leafpad ~/.bash_aliases
- alias start="bash /home/udooer/chimeres-orchestra/software/scripts/start_udoo.sh"
- alias stop="killall pd"
- alias setmum="scp -r <MYPATH>/chimeres-orchestra/software/puredata udooer@192.168.0.101:/home/udooer/chimeres-orchestra/software/"
- alias getmum="scp -r udooer@192.168.0.101:/home/udooer/chimeres-orchestra/software/puredata <MYPATH>/chimeres-orchestra/chimeres-orchestra/software/"
 

## Arduino
- Setup #1 : Udoo > Pd > Serial > Arduino DUE > MOSFETS > DC Motors
- Setup #2 : Raspberry Pi > Pd > Serial > Arduino MEGA > MOSFETS > DC Motors
- Setup #3 : Arduino MEGA > Ethernet Shield > MOSFETS > DC Motors

### Udoo : Serial
- Ouvrir le logiciel Arduino intégré de la distribution UDOObuntu (pas un autre à cause de la carte Arduino interne)
- Téléverser software/arduino/child-serial/child-serial.ino

Dans Pure Data, [comport] génère une erreur dans la console qui n'a pas d'importance : no serial devices found for "/dev/tty[ASU]*". Le nom de la carte Arduino n'est pas tout à fait composé de la même manière : "/dev/ttymxc3". 

Vérifier que l'utilisateur "udooer" soit bien dans le group "dialout" :
- getent group | grep dialout

Sinon : 
- sudo usermod -a -G dialout udooer

### Arduino : OSC (Ethernet Shield)
- Ouvrir Arduino version >= 1.8.11
- Installer la bibliothèque OSC - Adrian Freed (CNMAT - https://github.com/CNMAT/OSC)
- Ouvrir "software/arduino/child-osc/child-osc.ino"
- Changer l'ID si besoin, de 1 à 5, car l'ID 0 est celle utilisée par l'Arduino connectée en filaire (Serial) au mini-ordinateur.


## Clone de la SD Card
- sudo fdisk -l . output :  /dev/mmcblk0p1
- sudo umount /dev/mmcblk0
- sudo dd if=/dev/mmcblk0 of=~/udoobuntu-chimeres.img bs=4M conv=notrunc


## Pure Data

### Installation
- sudo apt-get install build-essential automake autoconf libtool gettext libasound2-dev tcl tk
- (si besoin: libftgl2 libjack-jackd2-dev)
- git clone https://github.com/pure-data/pure-data
- ./autogen.sh
- ./configure
- make
- sudo make install

Test:
- Lancer Pd et vérifier la version de Pd (pd -version)

### Ajouts des bibliothèques
Target UDOO  : armv7

- À partir de Pure Data : zexy, cyclone, iemguts, comport, iemlib (optionnel), ggee (optionnel)
- sudo apt-get install pd-iemlib (marche pas avec Deken, peut être nécessaire d'installer puredata puredata-core)
- Si zexy ne marche pas sudo apt-get install pd-zexy
- cp -R /usr/lib/pd/extra/iemlib ~/Pd/externals (pour déplacer iemlib au même endroit que les autres externals)
- "-lib" : zexy, iemlib1, iemlib2
- "-font-weight normal"
- Optionnel : malinette-abs pour le séquenceur

### Patch Chimères Orchestra
- Télécharger : wget https://framagit.org/resonance/chimeres-orchestra/-/archive/master/chimeres-orchestra-master.tar.gz (git clone n'a pas fonctionné sur la udoo ...)
- chmod a+x /home/udooer/chimeres-orchestra/software/scripts/start_udoo.sh


## Utilisation

### conf.txt
- NB_CHIMERES <1-6> : nombre de robots
- NB_ARMS <1-6> : nombre de bras par robot
- AUTO-BUILD <0-1> : construction d'objets automatique avec les bons nombres de bras et de robots
  - install.pd : [chimeres 3 6 18]
  - abs/setup.pd : [setup-group] et [setup-arm <AbsArmId>] 
  - abs/compozer.pd : [view-arm <AbsArmId>]
- DEBUG <0-1> : impression de messages dans la console

### Development mode
Copie de la UDOO vers ordinateur distant :
- scp -r udooer@192.168.0.101:/home/udooer/chimeres-orchestra/software/puredata ~/Bureau
- sftp://udooer@192.168.0.101

conf.txt :
- REMOTE_LOCAL <0-1> : communication en localhost entre [install.pd] et [install-remote.pd]
- COMPOZER-VIEW <0-1> : visualisation des bangs du compozer.pd.

### Install mode
- UDOO (mum) : install.pd
- Ordinateur distant : install-remote.pd, pour configurer les pattes (setup) et les compos (compozer)

conf.txt :
- CLOCK_START <0000-2359> : début d'activité de l'installation format hhmm
- CLOCK_END <0000-2359> : fin d'activité de l'installation format hhmm
- CLOCK_AUTO <0-1> : activation des horaires de début et de fin
- CLOCK <0-1> : activation de la minuterie, un événement tous les quarts d'heure.

### Live mode
- UDOO: child.pd
- Ordinateur distant : live-remote.pd

