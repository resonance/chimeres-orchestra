#! /bin/sh
# Launch Pure Data 

# Select the patch : main for UDOO (master) or children for other devices (slaves)
#patch="child.pd"
patch="install.pd"

# Options
args="-nomidi -noaudio -font-weight normal -open /home/udooer/chimeres-orchestra/software/puredata/$patch" 

# Pd 0.50.2 compiled (Feb. 2020)
pd $args &
