#! /bin/sh
# Launch Pure Data 

# The computer is the master (install) or a children
patch="child.pd"
#patch="install.pd"

args="-nomidi -noaudio -font-weight normal -open /home/pi/chimeres-orchestra/software/puredata/$patch" 

pd $args &
