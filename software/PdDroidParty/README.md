# Send OSC messages from smartphone to computer


## On a computer
- Change the patch and IP in "droidparty_main.pd"


## On Android
- install PdDroidParty.
- Create a folder "PdDroidParty" on Android root folder.
- Copy "chimeres-remote" in this folder.
- Open PdDroidParty app and the patch.

