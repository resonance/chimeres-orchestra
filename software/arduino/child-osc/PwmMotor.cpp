// Motor Class to control PWM of DC motors

#include "PwmMotor.h"

PwmMotor::PwmMotor(){
	state = false;
	last = 0;   
	time_on = 150;
	debug = false;
}

PwmMotor::~PwmMotor(){}

void PwmMotor::init(int _pin, int _time_max, boolean _debug = false){
	debug = _debug;
	time_on_max = _time_max;
	pin = _pin;
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
}

boolean PwmMotor::isOn(){
  return state; 
}

void PwmMotor::on(unsigned long _current, int _pwm, int _time){
	if( _current - last >= (time_on + 10) ) { // !state &&
    state = true;
    last = _current;
		time_on = constrain(_time, 0, time_on_max);
    if (debug) Serial.println("ON ");
		analogWrite(pin, _pwm);
	}
}

void PwmMotor::off(unsigned long _current) {
	if( _current - last >= time_on ) { // state &&
		state = false;
	  analogWrite(pin, 0);
    if (debug) Serial.println("OFF");
	}
}
