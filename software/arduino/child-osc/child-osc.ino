 /*
 * Chimères Orchestra
 * =================
 * Robots drummers in public space by Reso-nance Numérique
 * 
 * Website:
 * http://reso-nance.org/chimeres-orchestra 
 *
 * Setup: 
 * Ethernet (OSC) > Ethernet Shield > Arduino MEGA > Power Shield MOSFETS or 8 channel NPN board > DC Motors
 * 
 * Wiring:
 * Ethernet Shield  Arduino MEGA  Power Shield
 * ===============  ============  ============
 * 10               10 (53)     
 * 11               11 (51)
 * 12               12 (50)
 * 13               13 (52)
 * 5V               5V
 * GND              GND           GND
 *                  2             3
 *                  3             5
 *                  5             6
 *                  6             9
 *                  7             10
 *                  8             11
 * 
 * OSC message: 
 * /arm Id (int 0-5), PWM (int 0-255), Delay (int 0-100)
 * 
 * ID (from 1 to 5), IP and ports:
 * Static IP and port depends on the ID of the robot (chimere)
 * ID 0 depends on the other firmware child-serial.ino. It is the UDOO computer (192.168.0.101:9101) with an embedded Arduino.
 * ID 1 should be 192.168.0.102:9102
 * ID 2 should be 192.168.0.103:9103
 * and so on...
 * 
*/

#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <OSCBundle.h>
#include <OSCBoards.h>

#include "PwmMotor.h"

// ---- SET ID ---- //
const int id = 4; // Just change this number, from 1 to 5

// ----- DEBUG ----- //
const boolean DEBUG = false;

// ---- NETWORK ---- //
//byte ip[] = { 192, 168, 0, id + 101 }; // IP
IPAddress ip(192, 168, 0, id + 101);
const unsigned int port = id + 9101; // Receive port
byte mac[][6] = { // MAC addresses
  { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0x00 },
  { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0x01 },
  { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0x02 },
  { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0x03 },
  { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0x04 },
  { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0x05 },
  { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0x06 }
};
EthernetUDP udp;

// ---- CONFIGURATIONS ---- //
// Ethernet Shield use 10,11,12,13 pin and 4th pin for SD Card
const int PINS[] = { 2, 3, 5, 6, 7, 8}; 
const int TIME_MAX = 120;
const int NB = 6; // number of pins
const int TEMPO = 10; // sampling tempo, Ethernet delay

// ---- MOTORS --------- //
int motor_id, motor_pwm, motor_time;
PwmMotor motors[NB];
unsigned long current = 0;

// ---- SETUP ----------- //
void setup() {

   // Ethernet setup
   Ethernet.begin(mac[id], ip);
   //Ethernet.begin(mac[id]);

   if (DEBUG) Serial.begin(38400);
  
   // Check for Ethernet hardware present
   if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      if (DEBUG) Serial.println("Ethernet shield was not found. Sorry, can't run without hardware. :(");
      while (true) {
        delay(10); // do nothing, no point running without Ethernet hardware
      }
    }
 
   // Start UDP
   udp.begin(port);
   if (DEBUG) Serial.println("Start UDP");
  
   // Init pins
   for (int i=0; i < NB; i++) {
       motors[i].init(PINS[i], TIME_MAX, DEBUG);
    }
}

// ---- PROGRAM ----------- //
void loop() {
  // Update time
  current = millis();

  // Receive UDP packets
  OSCMessage msg;
  int msgSize = udp.parsePacket();
  
  if(msgSize > 0){
    while(msgSize > 0) {
      msg.fill(udp.read());
      msgSize--;
    }
    if(!msg.hasError()) {
      msg.route("/arm", routeMsg);
    }
  }

  // Motor Off
  for (int i=0; i < NB; i++) {
     if( motors[i].isOn() ) motors[i].off(current);
  }

  delay(TEMPO);
}

// ---- GET OSC --------- //
void routeMsg(OSCMessage &_msg, int _addrOffset){
 
  motor_id = _msg.getInt(0);
  motor_pwm = _msg.getInt(1);
  motor_time = _msg.getInt(2);

  if (DEBUG) {
    Serial.print(motor_id);
    Serial.print(" ");
    Serial.print(motor_pwm);
    Serial.print(" ");
    Serial.println(motor_time);
  }
 
  // Motor on
  if( motors[motor_id].isOn() == false ) motors[motor_id].on(current, motor_pwm, motor_time);
}
