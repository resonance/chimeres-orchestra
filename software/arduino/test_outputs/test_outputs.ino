/*
 * Chimères Orchestra
 * http://reso-nance.org/chimeres-orchestra 
 * 
 * Test 6 outputs (Mega)
 *
 * PWM Pins
 * - With power shield: 3, 5, 6, 9, 10, 11
 * - With NPN 8 channels: 2, 3, 5, 6, 7, 8. Avoid pin n°4 (Ethernet Shield).
*/

const int PINS[] = { 2, 3, 5, 6, 7, 8 } ; 
const int NB = 6;

void setup() {
  for (int i=0; i < NB; i++) pinMode(PINS[i], OUTPUT);
  delay(1000);
}

void loop() {
  for (int i=0; i < NB; i++) {
    analogWrite(PINS[i], 30);
    delay(50);
    analogWrite(PINS[i], 0);
    delay(500);
  }
}
