/*
 * Chimères Orchestra
 * http://reso-nance.org/chimeres-orchestra 
 *
 * Setup #1 : Udoo > Pd > Serial > Arduino DUE > MOSFETS > DC Motors
 * Setup #2 : Raspberry Pi > Pd > Serial > Arduino MEGA > MOSFETS > DC Motors
 * 
 * Serial message : 
 * - Door Lock Motors : Id (int 0-5), PWM (int 0-255), Delay (int 0-100)
 * - Stabdard DC Motor : Id (int 6), PWM (int 0-255), Delay (int). Delay is not used here but you need to put something for Serial parsing.
 *
 * Note on dc motors : 
 * pins 2, 3, 4, 5, 6, 7 are universal 12V door lock motors, a special lib is mandatory to not break them.
 * pin 8 is a standard 12V dc motor, which is controlled directly (analogWrite)
 * 
*/

// Special lib to manage time and avoid to break motors!
#include "PwmMotor.h"

// ---- MAIN SETUP ---- //

// Door Lock Motors
// mapping 6 channels Mosfets Power Shield from Sparkfun { 3, 5, 6, 9, 10, 11 }
// mapping 8 channels DIN Mosfets NPN Shield { 2, 3, 4, 5, 6, 7 }
const int PINS[] = { 2, 3, 4, 5, 6, 7 } ; 
const int NB = 6;

// Standard DC motor
const boolean DC_ACTIVE = true;
const int DC_PIN = 8;

// ---- SETUP ----- //
const int TIME_MAX = 130;
const int TEMPO = 5; // sampling tempo
const boolean DEBUG = false;
const boolean SENSOR_ACTIVE = false;

// ---- MOTORS --------- //
int motor_id, motor_pwm, motor_time;
PwmMotor motors[NB];
unsigned long current = 0;

// ----DIGITAL SENSOR (PIR) --------- //
/*
unsigned long button_interval = 50;
unsigned long button_previous = 0;
int button_state = 0;
int button_last_state = 0;
int button_pin = 2; //2,4,7,8,12,13 
*/

// ---- PROGRAM ----------- //
void setup() {
  Serial.begin(38400);

  // Door Lock Motors
   for ( int i = 0 ; i < NB ; i++ ) {
     motors[i].init(PINS[i], TIME_MAX, DEBUG);
  }

  // DC Motor
  if ( DC_ACTIVE ) {
    pinMode(DC_PIN, OUTPUT);
    digitalWrite(DC_PIN, LOW);
  }
}

void loop() {
  // UPDATE TIME
  current = millis();

/*
  // SENSOR
  if (SENSOR_ACTIVE) {
    if(current - button_previous > button_interval) { // sampling
     button_previous = current; 
     button_state = digitalRead(button_pin);
     if (button_state != button_last_state) { // if the state has changed
       Serial.println(button_state);
     }
     button_last_state = button_state;
    }
  }
*/

  // MOTOR ON
  while (Serial.available()) {
     motor_id = Serial.parseInt(); 
     motor_pwm = Serial.parseInt();
     motor_time = Serial.parseInt();
     if (Serial.read() == '\n') {
      // If there is the standard dc motor: direct control
      if ( DC_ACTIVE && ( motor_id == 6 ) ) analogWrite(DC_PIN, motor_pwm);
      
      // If the motor is off we can control it
      if( motors[motor_id].isOn() == false )  motors[motor_id].on(current, motor_pwm, motor_time);
     }
   }

  // MOTOR OFF
  for ( int i = 0; i < NB; i++) {
     if( motors[i].isOn() ) motors[i].off(current);
  }
  
  delay(TEMPO);
}
