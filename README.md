# Chimères Orchestra

- Website: http://reso-nance.org/chimeres-orchestra/
- Licence: GNU/GPL v3

## Description
All files to build and use the Chimères Orchestra artistic work.

## Build
See "./mechanics" and "./electronics" folders.

## Setup
See "./docs" folder and "SETUP.md" file.

### Installation mode

One mini computer (Udoo board) is connected to some Ethernet Shields and Arduino boards through Ethernet (RJ45) network. This computer is the "mum" and the other are "children".

- Mum is the Udoo computer : a bash script (software/scripts/start_udoo.sh) open "install.pd" patch.
- Child (ID:0) is the Arduino embedded in Udoo :  Udoo computer (ethernet) > Arduino DUE (child-serial.ino) > Power Shield Mosfets > motors.
- Children (ID:1,2,3,4,5) : Ethernet Shield > Arduino (child-osc.ino) > Power Shield Mosfets > motors
- The laptop computer is used to setup arms (power, delays) : open "install-remote.pd" patch.
- A smartphone (Android) can be used to make a simple controller : see "software/PdDroidParty" folder.

### Live mode

The Udoo computer becomes a "child" and a laptop is the main controller.

- Laptop : open "live-remote.pd" patch
- Child (ID:0) is the Udoo computer : a bash script (software/scripts/start_udoo.sh) open "child.pd" patch.
- Children (ID:1,2,3,4,5) : Ethernet Shield > Arduino (child-osc.ino) > Power Shield Mosfets > motors
